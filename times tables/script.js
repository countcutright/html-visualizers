'use strict';

var pi = Math.PI;
var Parser = new (new ParserLoad()).Parser;

var nextPointResult;
var prevExpressionValid = true;
function getNextPoint(idx){
    //return the index of the next point (result is abs(), modded, and floored to be valid)
    try {
        nextPointResult = Parser.evaluate(inputs.nextPointFunction.value, {i: idx, t: params.table});
        if (isFinite(nextPointResult) && nextPointResult != null){
        	prevExpressionValid = true;
            return nextPointResult;
        }
    } catch (err) {
    	if (prevExpressionValid){
        	console.log("invalid expr: \'",inputs.nextPointFunction.value,"\'");
        	prevExpressionValid = false;
    	}
    }
    return idx*params.table;
}

var params = {
    dynamicFillColor: true,
    dynamicStrokeColor: true,
    //static color settings
    fillColor: "#818181",
    strokeColor: "#818181",
    //all params below this line are set from the html values
    radius: null,
    points: 300,
    table: 30,
    tableDomUpdateRatio: 0.0003,
    speed: 1,
    lineWidth: 2.0,
    //dynamic color settings 
    colorPeriod: 50,
    minRed: 0,
    maxRed: 255,
    minGreen: 0,
    maxGreen: 255,
    minBlue: 0,
    maxBlue: 255,
};
var inputs = {
    radius: document.getElementById("radius"),
    points: document.getElementById("points"),
    table: document.getElementById("table"),
    speed: document.getElementById("speed"),
    animate: document.getElementById("animateTable"),
    lineWidth: document.getElementById("line-size"),
    showNumberLabelsCheckbox: document.getElementById("showNumberLabels"),
    nextPointFunction: document.getElementById("next-point-function"),
    color: {
    	red: document.getElementById("color-red"),
    	green: document.getElementById("color-green"),
    	blue: document.getElementById("color-blue"),
    	period: document.getElementById("color-period"),
    },
};

var main = document.getElementById("main"),
	cvs = document.getElementById("cvs"),
	ctx = cvs.getContext("2d"),
    cvs_background = document.getElementById("cvs_background"),
    ctx_background = cvs_background.getContext("2d"),
    cvs_number_labels = document.getElementById("number_labels"),
    ctx_numbers = cvs_number_labels.getContext("2d"),
	center = new Array(2),
	animating = false;
window.onload = function() {
	initCanvas();
    //init double range sliders
    var sliderSections = document.getElementsByClassName("range-slider-2");
    for (var x = 0; x < sliderSections.length; x++) {
        var sliders = sliderSections[x].getElementsByTagName("input");
        for (var y = 0; y < sliders.length; y++) {
            if (sliders[y].type === "range") {
                sliders[y].oninput = getSliderVals;
                sliders[y].oninput();
            }
        }
    }
    updateBackgroundCanvas = true;
    updateColorCache = true;
    console.log(can_v);
    console.log(ctx_v);
    draw();
}

function getStyle(obj) {
    return obj.currentStyle || window.getComputedStyle(obj);
}

var dt, prevTime, t_global = 0, prevTable, tableDomUpdateThreshold;
function animate(t) {
    if (animating)
        requestAnimationFrame(animate);
    else
        prevTime = 0;
    if (!prevTime)
        prevTime = t;
    dt = t - prevTime;
    prevTime = t;
    params.table = (params.table + 0.0002*dt*params.speed) % inputs.table.max;
    if (Math.abs(params.table - prevTable) > tableDomUpdateThreshold) {
        inputs.table.value = params.table;
        inputs.table.nextElementSibling.innerText = inputs.table.value;
        prevTable = params.table;
    }
    t_global = t;
    draw(t);
}

function getSliderVals() {
    var parent = this.parentNode,
    	slides = parent.getElementsByTagName("input"),
    	slide1 = parseFloat(slides[0].value),
    	slide2 = parseFloat(slides[1].value);
    if (slide1 > slide2) {
        var tmp = slide2;
        slide2 = slide1;
        slide1 = tmp;
    }
    var displayElement = parent.getElementsByClassName("rangeValues")[0];
    displayElement.setAttribute("min", slide1);
    displayElement.setAttribute("max", slide2);
    displayElement.oninput();
    displayElement.innerHTML = slide1 + " - " + slide2;
}

var addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
};
addEvent(window, "resize", function(event) {
	if (window.innerWidth < window.innerHeight) {
        cvs_number_labels.width = window.innerWidth;
        cvs.width = window.innerWidth - 2*parseFloat(getStyle(main).marginLeft);
    } else {
        cvs_number_labels.width = window.innerHeight;
        cvs.width = window.innerHeight - 2*parseFloat(getStyle(main).marginLeft);
    }
    cvs.height = cvs.width;
    cvs_background.width = cvs.width;
    cvs_background.height = cvs.height;
    cvs_number_labels.height = cvs_number_labels.width;
    can_v.height = cvs.height;
    can_v.width = cvs.width;
    can_v2.width = cvs_number_labels.width;
    can_v2.height = cvs_number_labels.height;
    params.radius = cvs.width / 3;
    center[0] = cvs.width / 2;
    center[1] = cvs.height / 2;
    inputs.radius.value = cvs.width/2 - 32*params.lineWidth/Math.sqrt(params.points);
    params.radius = inputs.radius.value;
    inputs.radius.nextElementSibling.innerText = params.radius;
    updateBackgroundCanvas = true;
    draw();
});

var can_v, ctx_v, can_v2, ctx_v2; //virtual canvas vars used for offscreen rendering
function initCanvas() {
    can_v = document.createElement('canvas');
    ctx_v = can_v.getContext('2d');
    can_v2 = document.createElement('canvas');
    ctx_v2 = can_v2.getContext('2d');
    if (window.innerWidth < window.innerHeight) {
        cvs_number_labels.width = window.innerWidth;
        cvs.width = window.innerWidth - 2*parseFloat(getStyle(main).marginLeft);
    } else {
        cvs_number_labels.width = window.innerHeight;
        cvs.width = window.innerHeight - 2*parseFloat(getStyle(main).marginLeft);
    }
    cvs_number_labels.height = cvs_number_labels.width;
    cvs.height = cvs.width;
    cvs_background.width = cvs.width;
    cvs_background.height = cvs.height;
    can_v.height = cvs.height;
    can_v.width = cvs.width;
    can_v2.height = cvs_number_labels.height;
    can_v2.width = cvs_number_labels.width;
    params.radius = cvs.width / 3;
    center[0] = cvs.width / 2;
    center[1] = cvs.height / 2;

    inputs.radius.value = cvs.width/2 - 32*params.lineWidth/Math.sqrt(params.points);
    params.radius = parseFloat(inputs.radius.value);
    params.points = parseFloat(inputs.points.value);
    if (params.points < 100) inputs.showNumberLabelsCheckbox.checked = true;
    else inputs.showNumberLabelsCheckbox.checked = false;
    inputs.table.max = parseFloat(inputs.points.value);
    params.table = parseFloat(inputs.table.value);
    prevTable = params.table;
    tableDomUpdateThreshold = params.tableDomUpdateRatio * params.points;
    params.speed = parseFloat(inputs.speed.value);
    params.lineWidth = parseFloat(inputs.lineWidth.value);
    params.colorPeriod = parseFloat(inputs.color.period.value * 100);
    //initialize display values
    inputs.radius.nextElementSibling.innerText = params.radius;
    inputs.points.nextElementSibling.innerText = params.points;
    inputs.table.nextElementSibling.innerText = params.table;
    inputs.speed.nextElementSibling.innerText = params.speed;
    inputs.lineWidth.nextElementSibling.innerText = params.lineWidth;
    inputs.color.period.nextElementSibling.innerText = params.colorPeriod / 100;

    inputs.radius.oninput = function(e) {
        params.radius = parseFloat(this.value);
        this.nextElementSibling.innerText = this.value;
        updateBackgroundCanvas = true;
        draw();
    };
    inputs.showNumberLabelsCheckbox.onclick = function(e) {
        if (this.checked){
            cvs_number_labels.style.display = "block"; //could be used for optimizations if prevPoints & prevRadius are tracked
            updateBackgroundCanvas = true;
            draw();
        } else {
            cvs_number_labels.style.display = "none";
        }
    };
    inputs.points.oninput = function(e) {
        params.points = parseFloat(this.value);
        inputs.table.max = params.points;
        this.nextElementSibling.innerText = this.value;
        updateBackgroundCanvas = true;
        prevTable = params.table;
        tableDomUpdateThreshold = params.tableDomUpdateRatio * params.points;
        draw();
    };
    inputs.table.oninput = function(e) {
        params.table = parseFloat(this.value);
        this.nextElementSibling.innerText = this.value;
        prevTable = params.table;
        tableDomUpdateThreshold = params.tableDomUpdateRatio * params.points;
        draw();
    };
    inputs.speed.oninput = function(e) {
        params.speed = parseFloat(this.value);
        this.nextElementSibling.innerText = this.value;
        draw();
    };
    inputs.lineWidth.oninput = function(e) {
        params.lineWidth = parseFloat(this.value);
        this.nextElementSibling.innerText = this.value;
        updateBackgroundCanvas = true;
        draw();
    };
    inputs.color.red.oninput = function(e) {
    	params.minRed = parseFloat(this.getAttribute("min"));
    	params.maxRed = parseFloat(this.getAttribute("max"));
        updateColorCache = true;
    };
    inputs.color.green.oninput = function(e) {
    	params.minGreen = parseFloat(this.getAttribute("min"));
    	params.maxGreen = parseFloat(this.getAttribute("max"));
        updateColorCache = true;
    };
    inputs.color.blue.oninput = function(e) {
    	params.minBlue = parseFloat(this.getAttribute("min"));
    	params.maxBlue = parseFloat(this.getAttribute("max"));
        updateColorCache = true;
    };
    inputs.color.period.oninput = function(e) {
    	params.colorPeriod = parseFloat(this.value)*100;
        this.nextElementSibling.innerText = this.value;
        updateColorCache = true;
    };
    inputs.animate.onclick = function() {
        animating = !animating;
        if (animating) {
            prevTime = 0;
            requestAnimationFrame(animate);
        }
    };
}

// avoid gc on common variables
var pointsCache = [], 
    p = new Array(2), p2 = new Array(2), pt = new Array(2),
    updateBackgroundCanvas = false,
    i, a, angleIncrement, 
    baseTextSizeHalf, textSizeHalf, margin, pointRadius; 
function renderBackgroundCanvas(){
    /*var dynamicColor = "";
    if (params.dynamicStrokeColor || params.dynamicFillColor)
        dynamicColor = getColor6(0, params.colorPeriod);
    if (params.dynamicStrokeColor)
        can_v.strokeStyle = dynamicColor;
    else
        can_v.strokeStyle = params.strokeColor;
    if (params.dynamicFillColor)
        can_v.fillStyle = dynamicColor;
    else
        can_v.fillStyle = params.strokeColor;*/
    ctx_v.clearRect(0, 0, can_v.width, can_v.height);
    ctx_v.strokeStyle = params.strokeColor;
    ctx_v.fillStyle = params.fillColor;
    ctx_v.lineWidth = params.lineWidth;
    if (inputs.showNumberLabelsCheckbox.checked) {
        margin = parseFloat(getStyle(main).margin);
        ctx_v2.clearRect(0, 0, can_v2.width, can_v2.height);
        ctx_v2.strokeStyle = ctx_v.strokeStyle;
        ctx_v2.fillStyle = ctx_v.fillStyle;
        ctx_v2.lineWidth = ctx_v.lineWidth;
        ctx_v2.textAlign = "center";
        ctx_v2.textBaseline = "middle";
    }
    // draw outer circle
    ctx_v.beginPath();
    ctx_v.arc(center[0], center[1], params.radius, 0, 2*pi);
    ctx_v.stroke();
    // precompute repeated calculations
    pointsCache.length = params.points;
    angleIncrement = 2*pi / params.points;
    baseTextSizeHalf = ctx_numbers.measureText(0).width/2;
    textSizeHalf = baseTextSizeHalf;
    pointRadius = 16*params.lineWidth/Math.sqrt(params.points);
    // draw points
    for (i = 0; i < params.points; i++) {
        a = i*angleIncrement;
        p[0] = center[0] + params.radius*Math.cos(a);
        p[1] = center[1] + params.radius*Math.sin(a);
        pointsCache[i] = [p[0], p[1]];
        ctx_v.beginPath();
        ctx_v.arc(p[0], p[1], pointRadius, 0, 2*pi);
        ctx_v.fill();
        if (inputs.showNumberLabelsCheckbox.checked) {
            if (i > 9)
                textSizeHalf = baseTextSizeHalf * (1 + Math.floor(Math.log10(i)));
            //p[0] = center[0] + margin + (can_v2.width/2 - textSizeHalf)*Math.cos(a);
            //p[1] = center[1] + margin + (can_v2.height/2 - textSizeHalf)*Math.sin(a);
            
            p[0] += margin + 1.3*(textSizeHalf + pointRadius)*Math.cos(a);
            p[1] += margin + 1.3*(baseTextSizeHalf + pointRadius)*Math.sin(a);
            ctx_v2.fillText(i, p[0], p[1]);
        }
    }
    ctx_background.clearRect(0, 0, cvs_background.width, cvs_background.height);
    ctx_background.drawImage(can_v, 0, 0);
    ctx_numbers.clearRect(0, 0, cvs_number_labels.width, cvs_number_labels.height);
    ctx_numbers.drawImage(can_v2, 0, 0);
}

var dynamicColor;
function draw(t) {
    if (t === NaN  || t === undefined)
        t = t_global;
    if (updateBackgroundCanvas) {
        renderBackgroundCanvas();
        updateBackgroundCanvas = false;
    }
    ctx_v.clearRect(0, 0, can_v.width, can_v.height);
    // coloring
    if (params.dynamicStrokeColor || params.dynamicFillColor)
    	dynamicColor = getColor6(t, params.colorPeriod);
    if (params.dynamicStrokeColor)
        ctx_v.strokeStyle = dynamicColor;
    else
        ctx_v.strokeStyle = params.strokeColor;
    if (params.dynamicFillColor)
        ctx_v.fillStyle = dynamicColor;
    else
        ctx_v.fillStyle = params.strokeColor;
    ctx_v.lineWidth = params.lineWidth;
/* handled by renderBackgroundCanvas
    // draw outer circle
    ctx.beginPath();
    ctx.arc(center.x, center.y, params.radius, 0, 2*pi);
    ctx.stroke();

    // draw points
    var i, a, p, p2, points = [];
    var angleIncrement = 2*pi / params.points;
    for (i = 0; i < params.points; i++) {
        a = i*angleIncrement;
        p = new Point(center.x + params.radius*cos(a), center.y + params.radius*sin(a));
        ctx.beginPath();
        ctx.arc(p.x, p.y, 16*params.lineWidth / sqrt(params.points), 0, 2*pi);
        ctx.fill();
        points.push(p);
    }*/
    // draw lines
    ctx_v.beginPath();
    for (i = 0; i < pointsCache.length; i++) {
        p[0] = pointsCache[i][0];
        p[1] = pointsCache[i][1];
        a = Math.floor(Math.abs(getNextPoint(i)) % params.points);
        p2[0] = pointsCache[a][0];
        p2[1] = pointsCache[a][1];
        if (pt[0] != p[0] || pt[1] != p[1])
            ctx_v.moveTo(p[0], p[1]);
        ctx_v.lineTo(p2[0], p2[1]);
        pt[0] = p2[0];
        pt[1] = p2[1];
    }
    ctx_v.stroke();
    ctx_v.closePath();

    ctx.clearRect(0, 0, cvs.width, cvs.height);
    ctx.drawImage(can_v, 0, 0);
}

var color6Cache = [],
    cacheSize,
    updateColorCache = false;
function getColor6(t, period) {
    if (period > cacheSize)
        t = Math.floor((t*cacheSize/period) % cacheSize);
    else
        t = Math.floor(t % cacheSize);
    if (updateColorCache) {
        if (period > 255*6)
            cacheSize = 255*6; //maximum number of colors using ints
        else 
            cacheSize = Math.floor(period);
        color6Cache.length = cacheSize;
        for (i = 0; i < cacheSize; i++) {
            color6Cache[i] = calculateColor6(i, cacheSize);
        }
        updateColorCache = false;
    }
    return color6Cache[t];
}

var red, green, blue, rgbstr,
    minRed, maxRed, minGreen, maxGreen, minBlue, maxBlue, 
    periodSplit;
function calculateColor6(t, period) {
    minRed = params.minRed;
    maxRed = params.maxRed;
    minGreen = params.minGreen;
    maxGreen = params.maxGreen;
    minBlue = params.minBlue;
    maxBlue = params.maxBlue;
    periodSplit = period/6;

    if (t <= periodSplit) { //red -> yellow
        red = maxRed;
        green = Math.round(minGreen + t/periodSplit*(maxGreen-minGreen)); //increasing
        if (green > 255) green = 255;
        blue = minBlue;
        //alpha = 0.6;
    } else if (t <= 2*periodSplit) { //yellow -> green
        red = Math.round(minRed + (2 - t/periodSplit)*(maxRed-minRed)); //decreasing
        if (red < 0) red = 0;
        green = maxGreen;
        blue = minBlue;
        //alpha = 0.6;
    } else if (t <= 3*periodSplit) { //green -> cyan
        red = minRed;
        green = maxGreen;
        blue = Math.round(minBlue + (t/periodSplit - 2)*(maxBlue-minBlue)); //increasing
        if (blue > 255) blue = 255;
        //alpha = 0.6;
    } else if (t <= 4*periodSplit) { //cyan -> blue
        red = minRed;
        green = Math.round(minGreen + (4 - t/periodSplit)*(maxGreen-minGreen)); //decreasing
        if (green < 0) green = 0;
        blue = maxBlue;
    } else if (t <= 5*periodSplit) { //blue -> magenta
        red = Math.round(minRed + (t/periodSplit - 4)*(maxRed-minRed)); //increasing
        if (red > 255) red = 255;
        green = minGreen;
        blue = maxBlue;
    } else { //magenta -> red
        red = maxRed;
        green = minGreen;
        blue = Math.round(minBlue + (period - t)/periodSplit*(maxBlue-minBlue)); //decreasing
        if (blue < 0) blue = 0;
    }
    return (rgbstr = "rgb("+red+","+green+","+blue+")");
}
